class Pin < ActiveRecord::Base
	
	Paperclip.options[:command_path] = "/usr/local/bin/"
	
	belongs_to :user

	has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }

	validates_attachment_content_type :image, :content_type => ['image/jpeg', 'image/jpg', 'image/png']

	validates :description, presence: true
	validates :image, presence: true
end
